# Requirements
* pipenv
* twitchio
* python-dotenv

# Install & Setup
* Clone the repo
* Install pipenv & required packages
* Create env variables
* Run the bot

## Clone the repo
Navigate to where you want to install the repo and clone into it

```bash
$ git clone git@gitlab.com:TReynolds/TheTwitchBot
```



## pipenv & required packages

```bash
$ pip install pipenv
```
Navigate into the 'TheTwitchBot' folder and setup pipenv
```bash
$ pipenv shell
$ exit
$ pipenv lock
$ pipenv install
```



## Create env variables

Navigate into the 'TheTwitchBot' folder and create the env file
```bash
$ touch .env
```
Edit the newly create .env file and add the following:
```txt
TMI_TOKEN=oauth:<OAUTH_TOKEN_HERE>
CLIENT_ID=<CLIENT_ID_HERE>
BOT_NICK=<THE_NAME_OF_BOT_ACCOUNT>
BOT_PREFIX=!
CHANNEL= <THE_NAME_OF_CHANNELS_CHAT_TO_JOIN>
```
**For Bot_Nick:**
This will be the name of the bot e.g. Nightbot, Cloudbot
* This will be the account name used when logging into Twitch to request the oauth token in the next step.
* If you desire a custom name for the bot, creating a new twitch account will be required and this will need to be used in the oauth request.

**For Oauth Token:**
[Request an oauth code](https://twitchapps.com/tmi/). You'll need to login and give the app permissions to generate it for you.

**For Client_ID:**
[Register your app with Twitch dev](https://dev.twitch.tv/console/apps/create) and request a client-id (so you can interface with Twitch's API)



## Run the bot

Your project should now have the following structure:
```bash
.ParentFolder
└── TheTwitchBot
   ├── .env
   ├── bot.py
   ├── Pipfile
   ├── Pipfile.lock
   └── README.md
```
From the 'TheTwitchBot' directory, run the following to start the bot
```bash
$ pipenv run python bot.py
```
If no errors are displayed, the bot should now be running :D

