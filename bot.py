import os

"""
----------------------------
        Custom Imports
----------------------------
"""
from helper import ModuleHelper
from twitchio.ext import commands, pubsub
from dotenv import load_dotenv

"""
-----------------------
        The Bot         
-----------------------
"""
class Bot(commands.Bot):
    load_dotenv(os.path.join(os.getcwd(), '.env'))
    def __init__(self):
        super().__init__(
            token=os.environ['TMI_TOKEN'],
            client_id=os.environ['CLIENT_ID'],
            nick=os.environ['BOT_NICK'],
            prefix=os.environ['BOT_PREFIX'],
            initial_channels=[os.environ['CHANNEL']])

    """
    ------------------------
            On Ready
    ------------------------
    """
    async def event_ready(self):
        print(f'Ready | {self.nick}', flush=True)
        channel = self.get_channel(os.environ['CHANNEL'])
        await channel.send("I am now online!")

    """
    --------------------------------
            Message Handling
    --------------------------------
    """
    async def event_message(self, message):
        if message.echo:
            return
        print("\n", flush=True)
        print("Author ID: " + message.author.id, flush=True)
        print("Author:    " + message.author.name, flush=True)
        print("Message:   " + message.content, flush=True)
        print("\n", flush=True)
        await self.handle_commands(message)
    
    

"""
--------------------------------
        Register the bot
--------------------------------
"""
if __name__ == "__main__":
    bot = Bot()
    availableModules = ModuleHelper.getAvailableModules()
    for x in availableModules:
        print("Initializing Module: " + x, flush=True)
        bot.load_module("modules."+x)
    bot.run()
