import os

"""
Get list of all available modules
"""
def getAvailableModules():
    baseDir = os.getcwd()
    module_dir = "modules/"
    all_files = os.listdir(baseDir + "/" + module_dir)
    formatted_file_names = [x[:-3] for x in all_files if(x not in ["__init__.py", "__pycache__"] and x[-3:] == ".py")]
    return formatted_file_names
