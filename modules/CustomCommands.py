import os
import re
import random
import json

from bot import Bot
from twitchio.ext import commands
from urllib.request import urlopen


class CustomCommands(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
    
    @commands.command(name="BadBrother")
    async def BadBrother(self, ctx: commands.Context):
        await ctx.send("@TheOriginalUDK2 is such a bad brother!")
        
    @commands.command(name="Pie")
    async def Pie(self, ctx: commands.Context):
        await ctx.send("Somebody fucked the pie!")

    @commands.command(name="original")
    async def originalSucks(self, ctx: commands.Context):
        await ctx.send("@TheOriginalUDK2 Sucks Peen")


    @commands.command(name="onlyfans")
    async def onlyFans(self, ctx: commands.Context):
        await ctx.send("monkaS")

    @commands.command(name="goodyear")
    async def goodyear(self, ctx: commands.Context):
        await ctx.send("No, The Worst!")

    @commands.command(name="insult")
    async def insult(self, ctx: commands.Context):
        insults = [
            ", if you were a spice you'd be flour!",
            ", your mother was a hamster and your father smells of elderberries!",
            ", do you know my favorite thing about you? Nothing!",
            ", if your IQ was 2 points higher, you'd be a rock!",
            ", you're not a complete idiot, but you will do until one comes long.",
            ", I like Rune more than you!",
        ]

        channel = self.bot.get_channel(os.environ["CHANNEL"])
        viewerListUrl = (
            "https://tmi.twitch.tv/group/user/" + str(channel.name) + "/chatters"
        )
        response = urlopen(viewerListUrl)
        allViewData = json.loads(response.read())
        viewers = allViewData["chatters"]["moderators"]
        viewers += allViewData["chatters"]["vips"]
        viewers += allViewData["chatters"]["viewers"]

        regexString = "\!insult.(\@\w+)"
        doesMatch = re.match(regexString, ctx.message.content)
        if doesMatch:
            for user in viewers:
                if doesMatch.groups()[0][1:].lower() == user:
                    if user == "a_happy_bot":
                        await ctx.send(
                            "How original @"
                            + str(ctx.message.author.name)
                            + ", trying to insult the bot... You must be really stupid thinking that this was going to work!"
                        )                
                    else:
                        await ctx.send("@" + user + random.choice(insults))
        else:
            await ctx.send(ctx.message.author.name + str(" is the absolute worst!"))


def prepare(bot: commands.Bot):
    bot.add_cog(CustomCommands(bot))
