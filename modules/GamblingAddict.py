import os
import re
import random
import pickle
import pathlib
import time
import threading
import json

baseDir = "modules/accounts/"

from urllib.request import urlopen
from twitchio.ext import commands

"""
Create accounts data folder
"""
pathlib.Path(baseDir[:-1]).mkdir(exist_ok=True)


class Gambler(object):
    def __init__(self, GamblerID, GamblerName, GamblerAcorns):
        self.username = GamblerName
        self.userID = GamblerID
        self.currentAcorns = GamblerAcorns
        self.accountPath = baseDir + self.userID

    def timedUpdate(self):
        self.currentAcorns += 10
        self.updateAccount()

    def updateAcorns(self, moreAcorns):
        self.currentAcorns += moreAcorns
        self.updateAccount()

    def updateAccount(self):
        pickle.dump(self, open(self.accountPath, "wb"))

    def getUsername(self):
        return self.username

    def getUserID(self):
        return self.userID

    def getCurrentAcorns(self):
        return self.currentAcorns

    def setCurrentAcorns(self, acornAmount):
        self.currentAcorns = acornAmount
        self.updateAccount()


class GamblingAddict(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    """
    --------------------------------
            Message Handling
    --------------------------------
    """

    @commands.Cog.event()
    async def event_message(self, message):
        channel = self.bot.get_channel(os.environ["CHANNEL"])
        if message.echo:
            return
        # Temporary block to bail someone out when hitting 0
        #
        # This will be removed for a more elegant solution when custom gambling
        # is fully implemented
        if message.author.name == "streamlabs":
            regexString = "Rolled\s\d+,\s(\@\w+)(.lost.\d*.Acorns.and.now.has.0.Acorns)"
            regexMatches = re.match(regexString, message.content)
            try:
                result = regexMatches.groups()
                if result:
                    result = result[0]
                    # await time.sleep(1)
                    await channel.send(
                        "Uh Oh... Looks like someone went bankrupt! Sending Bail Out Fund!"
                    )
                    # await time.sleep(2)
                    await channel.send("!addpoints " + result + " 100")
            except AttributeError as e:
                return

    def gamblePoints(self, player_points):
        # print("Die Rolled")
        rolled_val = random.randint(1, 100)
        print("Random Roll:\t{}".format(rolled_val), flush=True)
        gambled_amount = player_points
        print("Gambled Amount:\t{}".format(gambled_amount), flush=True)

        gamble_multiplier = 0
        if rolled_val <= 66:
            print("66% Chance Lost", flush=True)
            """Do Nothing"""
        elif rolled_val <= 99:
            print("33% Chance Win", flush=True)
            gamble_multiplier = 1
        elif rolled_val == 100:
            print("JACKPOT!!", flush=True)
            gamble_multiplier = 4

        return gambled_amount * gamble_multiplier, rolled_val

    def ViewerTimer(self):
        channel = self.bot.get_channel(os.environ["CHANNEL"])
        viewerListUrl = (
            "https://tmi.twitch.tv/group/user/" + str(channel.name) + "/chatters"
        )
        response = urlopen(viewerListUrl)
        allViewData = json.loads(response.read())
        viewers = allViewData["chatters"]["moderators"]
        viewers += allViewData["chatters"]["vips"]
        viewers += allViewData["chatters"]["viewers"]

        for x in viewers:
            try:
                theGambler = GambleHelper.getAccountFromName(x)
                theGambler.timedUpdate()
            except AttributeError as e:
                """Do Nothing"""
        self.viewerThread.run()

    @commands.Cog.event()
    async def event_ready(self):
        self.viewerThread = threading.Timer(300, self.ViewerTimer)
        self.viewerThread.daemon = True
        self.viewerThread.start()

    @commands.command(name="addict")
    async def addict(self, ctx: commands.Context):
        responses = [
            "is one hell of a gambler!",
            "is spiralling into a problem...",
            "has gambled 1 too many times!",
            "needs to stop!",
        ]
        regexString = "\!addict.(\@\w+)"
        doesMatch = re.match(regexString, ctx.message.content)
        if doesMatch:
            await ctx.send(doesMatch.groups()[0] + " " + random.choice(responses))
        else:
            await ctx.send("You are an addict, " + ctx.message.author.name + "!")

    @commands.command(name="gamble")
    async def gamble(self, ctx: commands.Context):
        StringBuilder = ""
        regexString = "\!gamble.(all|\d+)"
        doesMatch = re.match(regexString, ctx.message.content)
        if doesMatch:
            tempGambler = GambleHelper.readOrNewAccount(baseDir, ctx.message.author)

            result = doesMatch.groups()[0]
            if result == "all" and tempGambler.getCurrentAcorns() > 0:
                StringBuilder += "@" + ctx.message.author.name + " has gambled ALL their acorns!!"
                await ctx.send(StringBuilder)
                StringBuilder = ""
                print("Gambling all", flush=True)
                gambleVals = self.gamblePoints(tempGambler.getCurrentAcorns())
                gamble_payout = gambleVals[0]
                rolled_val = gambleVals[1]
                gamblersAcorns = tempGambler.getCurrentAcorns()

                if gamble_payout == 0:
                    StringBuilder += " And THEY LOST IT ALL!!  Too Bad @" + ctx.message.author.name + ", better luck next time."
                    tempGambler.setCurrentAcorns(0)
                else:
                    """This will be the jackpot output"""
                    if False:
                        print("Not Here", flush=True)
                    else:
                        StringBuilder += " you rolled a " + str(rolled_val) + " and won " + str(gamble_payout) + " acorns. "
                        finalAcorns = gamblersAcorns + gamble_payout
                        tempGambler.setCurrentAcorns(finalAcorns)
                        StringBuilder += "You now have " + str(tempGambler.getCurrentAcorns()) + " acorns."
                        
            elif result == "all" and tempGambler.getCurrentAcorns() == 0:
                StringBuilder += "Sorry buddy, but it looks like you don't have any acorns :( Come back later when you have do @" + ctx.message.author.name +"."
                await ctx.send(StringBuilder)
                StringBuilder = ""

            else:
                if int(result) > tempGambler.getCurrentAcorns():
                    StringBuilder += "You have gambled more acorns than you currently have @" + ctx.message.author.name + ". "
                    print("Gambling more Acorns than you currently have", flush=True)
                    StringBuilder += "You currently have " + str(tempGambler.getCurrentAcorns()) + " acorns."
                    print("You currently have: " + str(tempGambler.getCurrentAcorns()) + " acorns", flush=True)
                else:
                    StringBuilder += "Gambling " + result + " acorns. "
                    await ctx.send(StringBuilder)
                    StringBuilder = ""
                    print("Gambling " + result, flush=True)
                    gambleVals = self.gamblePoints(int(result))
                    gamble_payout = gambleVals[0]
                    rolled_val = gambleVals[1]
                    gamblersAcorns = tempGambler.getCurrentAcorns()
                    StringBuilder += "@"+ctx.message.author.name + " rolled a " + str(rolled_val) + " and "

                    if gamble_payout == 0:
                        StringBuilder += " LOST!! "
                        print("You Lost")
                        tempGambler.setCurrentAcorns(gamblersAcorns - int(result))
                    else:
                        """This will be the jackpot output"""
                        if False:
                            print("Not here", flush=True)
                        else:
                            StringBuilder += "WON!! "
                            finalAcorns = gamblersAcorns + gamble_payout
                            tempGambler.setCurrentAcorns(finalAcorns)
                            StringBuilder += "You won " + str(gamble_payout) + " acorns. "
                            print("Gambler won: "+str(gamble_payout), flush=True)

                    StringBuilder += "You now have " + str(tempGambler.getCurrentAcorns()) + " acorns."
                    print("Gambler now has: " + str(tempGambler.getCurrentAcorns()) + " acorns", flush=True)
            await ctx.send(StringBuilder)

    @commands.command(name="currentAcorns")
    async def currentAcorns(self, ctx: commands.Context):
        user = ctx.message.author

        tempGambler = GambleHelper.readOrNewAccount(baseDir, user)
        print(user.name+"'s current acorns: "+str(tempGambler.getCurrentAcorns()), flush=True)

    @commands.command(name="addpoints")
    async def addPoints(self, ctx: commands.Context):
        author = ctx.message.author
        if author.is_mod:
            regexString = "\!addpoints.(\@\w+).(\d+)"
            doesMatch = re.match(regexString, ctx.message.content)
            
            userString = doesMatch.groups()[0]
            user = userString[1:]
            
            pointsString = doesMatch.groups()[1]
            points = int(pointsString)

            targetUserList = await self.bot.fetch_users(names=[user])
            targetUser = targetUserList[0]
            
            targetGambler = GambleHelper.readOrNewAccount(baseDir, targetUser)
            targetGambler.updateAcorns(points)
        else:
            print(author.name + " is not a mod", flush=True)


def prepare(bot: commands.Bot):
    bot.add_cog(GamblingAddict(bot))


class GambleHelper:
    def readOrNewAccount(path, user):
        path = path + str(user.id)
        try:
            existingAccount = pickle.load(open(path, "rb"))
        except (OSError, IOError) as e:
            existingAccount = Gambler(user.id, user.name, 1)
            pickle.dump(existingAccount, open(path, "wb"))
        return existingAccount

    def getAccountFromName(username):
        accountList = os.listdir(baseDir)
        for acc in accountList:
            tempAcc = pickle.load(open(baseDir + acc, "rb"))
            # print("username: " + tempAcc.getUsername(), flush=True)
            if username == tempAcc.getUsername():
                """print(
                    "\nUsername: {} | Account Found: {}\n".format(
                        username, tempAcc.getUsername()
                    ),
                    flush=True,
                )"""
                return tempAcc
